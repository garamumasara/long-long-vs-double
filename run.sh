#!/bin/bash
TIMEFORMAT=' %Rsec'
for _ in {1..10}
do
    echo -n "./long_int"
    time ./long_int
done
for _ in {1..10}
do
    echo -n "./double"
    time ./double
done
for _ in {1..10}
do
    echo -n "./double_sse4_2"
    time ./double_sse4_2
done
for _ in {1..10}
do
    echo -n "./double_avx"
    time ./double_avx
done
for _ in {1..10}
do
    echo -n "./double_avx2"
    time ./double_avx2
done
unset TIMEFORMAT
