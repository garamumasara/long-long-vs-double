# long long vs double

## usage

```shell
$ make run
gcc -o long_int -Ofast long_int.c
gcc -o double -Ofast double.c
gcc -o double_sse4_2 -Ofast -msse4.2 double.c
gcc -o double_avx -Ofast -mavx double.c
gcc -o double_avx2 -Ofast -mavx2 double.c
./run.sh
./long_int 2.409sec
./long_int 2.407sec
./long_int 2.505sec
./long_int 2.406sec
./long_int 2.433sec
./long_int 2.405sec
./long_int 2.432sec
./long_int 2.407sec
./long_int 2.433sec
./long_int 2.408sec
./double 3.647sec
./double 3.610sec
./double 3.646sec
./double 3.611sec
./double 3.647sec
./double 3.615sec
./double 3.647sec
./double 3.613sec
./double 3.647sec
./double 3.616sec
./double_sse4_2 3.648sec
./double_sse4_2 3.617sec
./double_sse4_2 3.647sec
./double_sse4_2 3.618sec
./double_sse4_2 3.649sec
./double_sse4_2 3.618sec
./double_sse4_2 3.648sec
./double_sse4_2 3.617sec
./double_sse4_2 3.650sec
./double_sse4_2 3.617sec
./double_avx 1.828sec
./double_avx 1.815sec
./double_avx 1.828sec
./double_avx 1.823sec
./double_avx 1.809sec
./double_avx 1.814sec
./double_avx 1.827sec
./double_avx 1.825sec
./double_avx 1.829sec
./double_avx 1.828sec
./double_avx2 1.797sec
./double_avx2 1.828sec
./double_avx2 1.825sec
./double_avx2 1.796sec
./double_avx2 1.829sec
./double_avx2 1.794sec
./double_avx2 1.827sec
./double_avx2 1.826sec
./double_avx2 1.828sec
./double_avx2 1.826sec
```

## required

bash, gcc, gnumake
