run : long_int double double_sse4_2 double_avx double_avx2
	./run.sh

long_int : long_int.c
	gcc -o $@ -Ofast $<

double : double.c
	gcc -o $@ -Ofast $<

double_sse4_2 : double.c
	gcc -o $@ -Ofast -msse4.2 $<

double_avx : double.c
	gcc -o $@ -Ofast -mavx $<

double_avx2 : double.c
	gcc -o $@ -Ofast -mavx2 $<

clean :
	rm -f long_int double double_sse4_2 double_avx double_avx2

.PHONY : run clean
